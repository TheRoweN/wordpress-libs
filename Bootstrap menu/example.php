 <?php
 /* Include this code in your header or custom menu location */
$defaults = array(
        'theme_location'  => 'primary',
        'menu'            => '',
        'container'       => '',
        'container_class' => '',
        'container_id'    => '',
        'menu_class'      => '',
        'menu_id'         => '',
        'echo'            => true,
        'fallback_cb'     => 'wp_page_menu',
        'before'          => '',
        'after'           => '',
        'link_before'     => '', 
        'link_after'      => '',
        /* Css Class of the menu */
        'items_wrap'      => '<ul id="%1$s" class="nav navbar-nav navbar-right" role="menu">%3$s</ul>', 
        'depth'           => 0,
        /* Load the class */
        'walker'          => new wp_bootstrap_navwalker()
);
/* Show the menu */
wp_nav_menu( $defaults );
?>